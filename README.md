## Testes automatizados em Go!

### Introdução

#### O que é?
- Em Go, os testes automatizados referem-se à prática de escrever e executar testes de unidade, integração ou de qualquer outra natureza de forma automatizada usando as ferramentas e bibliotecas disponíveis na linguagem Go. Esses testes são escritos para garantir que o código funcione corretamente, identificar regressões após alterações no código e manter a qualidade do software ao longo do tempo.

#### Onde posso entender os conceitos relacionados a testes?
- Recomendo que procure por Test-Driven Development (TDD) [Artigo Aqui](https://www.devmedia.com.br/test-driven-development-tdd-simples-e-pratico/18533)

#### Como posso fazer testes em meus projetos Go?
- Crie arquivos de teste com o sufixo _test.go. Dentro desses arquivos, importe o pacote testing e escreva funções de teste que começam com Test. Essas funções devem usar a estrutura *testing.T para relatar falhas nos testes.

#### Exemplo
```go
// math_test.go
package math

import "testing"

func TestAdd(t *testing.T) {
    result := Add(2, 3)
    expected := 5
    if result != expected {
        t.Errorf("Expected %d, but got %d", expected, result)
    }
}
```

#### Executando...
Para executar apenas um teste específico ou um conjunto de testes, você pode fornecer argumentos adicionais ao <i><b>go test</b></i>, especificando o nome dos testes ou padrões de nome de teste.

```go test -run TestNomeDoTeste```

Para exibir saída detalhada sobre a execução dos testes, use a opção -v (verbose):

```go test -v```

#### Cobertura de teste...

Para verificar a cobertura de teste do seu código, você pode usar a flag -cover com go test:

```go test -cover```

#### Salvando os resultados...

```go test -coverprofile=cover.txt```

Salvando em um arquivo html(Amo isso):

```go tool cover -html=cover.txt -o cover.html```

#### Benchmarks...

Se você tiver testes de benchmark, pode executá-los usando a opção -bench:

```go test -bench=.```

#### Testes em subdiretórios...

Para executar testes em todos os subdiretórios de um diretório, você pode usar a opção ./...:

```go test ./...```

Fontes: [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-write-unit-tests-in-go-using-go-test-and-the-testing-package) e [GoLang Doc](https://pkg.go.dev/testing)